        name := "testAndGeneratePdf"
scalaVersion := "2.12.3"
watchSources += baseDirectory.value / "tex" / "main.tex"
watchSources += baseDirectory.value / "tex" / "slides.tex"
watchSources += baseDirectory.value / "tex" / "handouts.tex"
 showSuccess := false
